﻿using System.Windows.Forms;
using System.Data;
namespace e_Tag_Write
{
    partial class Write_tag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (this.dt_printdata != null && this.dt_printdata.Rows.Count > 0)
            {
                string tags = "";
                foreach (DataRow dr in dt_printdata.Rows)
                {
                    tags = tags + dr["print3"].ToString() + ", ";
                }
              //  MessageBox.Show("Printing Data :" + tags + " are pending to print", "Information");
                string message = "Tag No:" + tags + " are pending to print \n \nDo you want to Print and close?";
                string title = "Warning!";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                if (result == DialogResult.Yes)
                {
                    btnPrint_Sticker();
                    dt_printdata = null;
                    base.Dispose(disposing);
                }
                else
                {
                    base.Dispose(disposing);
                }
            }
            else
                base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Write_tag));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbItemType = new System.Windows.Forms.ComboBox();
            this.btnAddItmType = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtStone0 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtStone1 = new System.Windows.Forms.RadioButton();
            this.chk_Actual_Stone = new System.Windows.Forms.CheckBox();
            this.rbtStone2 = new System.Windows.Forms.RadioButton();
            this.rbtStone3 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStone_details_R = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtStone_RVal = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStone_details_G = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtStone_GVal = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStone_details_W = new System.Windows.Forms.TextBox();
            this.txtStone_WVal = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtStone_details_K = new System.Windows.Forms.TextBox();
            this.txtStone_KVal = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtExtraTag = new System.Windows.Forms.TextBox();
            this.chkExtratag = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtGrossWt = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtHalmarkNo = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtRFID_Tag_Weight = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbTagType = new System.Windows.Forms.ComboBox();
            this.btnAddTagType = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTagNo = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTagID = new System.Windows.Forms.Label();
            this.btnTagPrint = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnWriteSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImgClear = new System.Windows.Forms.Button();
            this.btnUploadImage = new System.Windows.Forms.Button();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComboBox_baud = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ComboBox_AlreadyOpenCOM = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.ComboBox_baud2 = new System.Windows.Forms.ComboBox();
            this.btnComOpen = new System.Windows.Forms.Button();
            this.cmbComPort = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.Edit_ComAdr = new System.Windows.Forms.TextBox();
            this.Edit_dmaxfre = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Edit_dminfre = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Edit_powerdBm = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.EPCC1G2 = new System.Windows.Forms.CheckBox();
            this.ISO180006B = new System.Windows.Forms.CheckBox();
            this.Edit_scantime = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Edit_Version = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Edit_Type = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.lblHexCode = new System.Windows.Forms.Label();
            this.lblAsciiCode = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.txtKar_melting = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtKar_wastage = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDirect_melting = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDirect_wastage = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel12, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel13, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel14, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel15, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel16, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel17, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel19, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.870998F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.830025F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.303249F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.581227F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.844766F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.303249F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.36462F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.71855F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1247, 554);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.29F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.29F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.42F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmbItemType, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnAddItmType, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(614, 40);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(5, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "ItemType";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbItemType
            // 
            this.cmbItemType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbItemType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cmbItemType.FormattingEnabled = true;
            this.cmbItemType.Location = new System.Drawing.Point(221, 5);
            this.cmbItemType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbItemType.Name = "cmbItemType";
            this.cmbItemType.Size = new System.Drawing.Size(298, 28);
            this.cmbItemType.TabIndex = 1;
            this.cmbItemType.SelectedIndexChanged += new System.EventHandler(this.cmbItemType_SelectedIndexChanged);
            // 
            // btnAddItmType
            // 
            this.btnAddItmType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddItmType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAddItmType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAddItmType.Location = new System.Drawing.Point(528, 5);
            this.btnAddItmType.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddItmType.Name = "btnAddItmType";
            this.btnAddItmType.Size = new System.Drawing.Size(81, 30);
            this.btnAddItmType.TabIndex = 2;
            this.btnAddItmType.Text = "+";
            this.btnAddItmType.UseVisualStyleBackColor = true;
            this.btnAddItmType.Click += new System.EventHandler(this.btnAddItmType_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.84473F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.56007F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.03882F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.29945F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.25693F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel3.Controls.Add(this.rbtStone0, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbtStone1, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.chk_Actual_Stone, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbtStone2, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbtStone3, 4, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(628, 5);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(614, 40);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // rbtStone0
            // 
            this.rbtStone0.AutoSize = true;
            this.rbtStone0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtStone0.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rbtStone0.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbtStone0.Location = new System.Drawing.Point(127, 5);
            this.rbtStone0.Margin = new System.Windows.Forms.Padding(4);
            this.rbtStone0.Name = "rbtStone0";
            this.rbtStone0.Size = new System.Drawing.Size(81, 30);
            this.rbtStone0.TabIndex = 23;
            this.rbtStone0.TabStop = true;
            this.rbtStone0.Text = "0";
            this.rbtStone0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtStone0.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(5, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 38);
            this.label2.TabIndex = 0;
            this.label2.Text = "No of Stones";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbtStone1
            // 
            this.rbtStone1.AutoSize = true;
            this.rbtStone1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtStone1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rbtStone1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbtStone1.Location = new System.Drawing.Point(217, 5);
            this.rbtStone1.Margin = new System.Windows.Forms.Padding(4);
            this.rbtStone1.Name = "rbtStone1";
            this.rbtStone1.Size = new System.Drawing.Size(89, 30);
            this.rbtStone1.TabIndex = 2;
            this.rbtStone1.TabStop = true;
            this.rbtStone1.Text = "1";
            this.rbtStone1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtStone1.UseVisualStyleBackColor = true;
            // 
            // chk_Actual_Stone
            // 
            this.chk_Actual_Stone.AutoSize = true;
            this.chk_Actual_Stone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk_Actual_Stone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Actual_Stone.Location = new System.Drawing.Point(517, 4);
            this.chk_Actual_Stone.Name = "chk_Actual_Stone";
            this.chk_Actual_Stone.Size = new System.Drawing.Size(93, 32);
            this.chk_Actual_Stone.TabIndex = 22;
            this.chk_Actual_Stone.Text = "Pair";
            this.chk_Actual_Stone.UseVisualStyleBackColor = true;
            // 
            // rbtStone2
            // 
            this.rbtStone2.AutoSize = true;
            this.rbtStone2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtStone2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rbtStone2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbtStone2.Location = new System.Drawing.Point(315, 5);
            this.rbtStone2.Margin = new System.Windows.Forms.Padding(4);
            this.rbtStone2.Name = "rbtStone2";
            this.rbtStone2.Size = new System.Drawing.Size(85, 30);
            this.rbtStone2.TabIndex = 3;
            this.rbtStone2.TabStop = true;
            this.rbtStone2.Text = "2";
            this.rbtStone2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtStone2.UseVisualStyleBackColor = true;
            // 
            // rbtStone3
            // 
            this.rbtStone3.AutoSize = true;
            this.rbtStone3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtStone3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.rbtStone3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rbtStone3.Location = new System.Drawing.Point(409, 5);
            this.rbtStone3.Margin = new System.Windows.Forms.Padding(4);
            this.rbtStone3.Name = "rbtStone3";
            this.rbtStone3.Size = new System.Drawing.Size(100, 30);
            this.rbtStone3.TabIndex = 4;
            this.rbtStone3.TabStop = true;
            this.rbtStone3.Text = "3";
            this.rbtStone3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtStone3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel4, 2);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.46931F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.88574F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.43532F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.24646F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.24646F));
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel6, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel8, 4, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(5, 54);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1237, 45);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(5, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(206, 43);
            this.label3.TabIndex = 0;
            this.label3.Text = "Stone Details";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Red;
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtStone_details_R, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label23, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtStone_RVal, 3, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(220, 5);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(223, 35);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(4, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 35);
            this.label4.TabIndex = 0;
            this.label4.Text = "R";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtStone_details_R
            // 
            this.txtStone_details_R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStone_details_R.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtStone_details_R.Location = new System.Drawing.Point(48, 4);
            this.txtStone_details_R.Margin = new System.Windows.Forms.Padding(4);
            this.txtStone_details_R.Name = "txtStone_details_R";
            this.txtStone_details_R.Size = new System.Drawing.Size(47, 26);
            this.txtStone_details_R.TabIndex = 5;
            this.txtStone_details_R.Text = "0.0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label23.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label23.Location = new System.Drawing.Point(102, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 35);
            this.label23.TabIndex = 6;
            this.label23.Text = "R Val.";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtStone_RVal
            // 
            this.txtStone_RVal.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtStone_RVal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStone_RVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.txtStone_RVal.Location = new System.Drawing.Point(168, 2);
            this.txtStone_RVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtStone_RVal.Name = "txtStone_RVal";
            this.txtStone_RVal.Size = new System.Drawing.Size(52, 22);
            this.txtStone_RVal.TabIndex = 7;
            this.txtStone_RVal.Text = "0.0";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.Green;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtStone_details_G, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label24, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtStone_GVal, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(452, 5);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(255, 35);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(4, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 35);
            this.label5.TabIndex = 0;
            this.label5.Text = "G";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtStone_details_G
            // 
            this.txtStone_details_G.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStone_details_G.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtStone_details_G.Location = new System.Drawing.Point(55, 4);
            this.txtStone_details_G.Margin = new System.Windows.Forms.Padding(4);
            this.txtStone_details_G.Name = "txtStone_details_G";
            this.txtStone_details_G.Size = new System.Drawing.Size(55, 26);
            this.txtStone_details_G.TabIndex = 6;
            this.txtStone_details_G.Text = "0.0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label24.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label24.Location = new System.Drawing.Point(117, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 35);
            this.label24.TabIndex = 7;
            this.label24.Text = "G Val.";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtStone_GVal
            // 
            this.txtStone_GVal.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtStone_GVal.Location = new System.Drawing.Point(193, 2);
            this.txtStone_GVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtStone_GVal.Name = "txtStone_GVal";
            this.txtStone_GVal.Size = new System.Drawing.Size(49, 22);
            this.txtStone_GVal.TabIndex = 8;
            this.txtStone_GVal.Text = "0.0";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtStone_details_W, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtStone_WVal, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label25, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(716, 5);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(252, 35);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(4, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 35);
            this.label6.TabIndex = 0;
            this.label6.Text = "W";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtStone_details_W
            // 
            this.txtStone_details_W.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStone_details_W.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtStone_details_W.Location = new System.Drawing.Point(54, 4);
            this.txtStone_details_W.Margin = new System.Windows.Forms.Padding(4);
            this.txtStone_details_W.Name = "txtStone_details_W";
            this.txtStone_details_W.Size = new System.Drawing.Size(55, 26);
            this.txtStone_details_W.TabIndex = 7;
            this.txtStone_details_W.Text = "0.0";
            // 
            // txtStone_WVal
            // 
            this.txtStone_WVal.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtStone_WVal.Location = new System.Drawing.Point(191, 2);
            this.txtStone_WVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtStone_WVal.Name = "txtStone_WVal";
            this.txtStone_WVal.Size = new System.Drawing.Size(49, 22);
            this.txtStone_WVal.TabIndex = 8;
            this.txtStone_WVal.Text = "0.0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label25.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label25.Location = new System.Drawing.Point(116, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 35);
            this.label25.TabIndex = 9;
            this.label25.Text = "W Val.";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel8.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtStone_details_K, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtStone_KVal, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.label26, 2, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(977, 5);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(255, 35);
            this.tableLayoutPanel8.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(4, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 35);
            this.label7.TabIndex = 0;
            this.label7.Text = "K";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtStone_details_K
            // 
            this.txtStone_details_K.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStone_details_K.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtStone_details_K.Location = new System.Drawing.Point(55, 4);
            this.txtStone_details_K.Margin = new System.Windows.Forms.Padding(4);
            this.txtStone_details_K.Name = "txtStone_details_K";
            this.txtStone_details_K.Size = new System.Drawing.Size(55, 26);
            this.txtStone_details_K.TabIndex = 8;
            this.txtStone_details_K.Text = "0.0";
            // 
            // txtStone_KVal
            // 
            this.txtStone_KVal.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txtStone_KVal.Location = new System.Drawing.Point(193, 2);
            this.txtStone_KVal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtStone_KVal.Name = "txtStone_KVal";
            this.txtStone_KVal.Size = new System.Drawing.Size(49, 22);
            this.txtStone_KVal.TabIndex = 9;
            this.txtStone_KVal.Text = "0.0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label26.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label26.Location = new System.Drawing.Point(117, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 35);
            this.label26.TabIndex = 10;
            this.label26.Text = "K Val.";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.42857F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.61905F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.66667F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66667F));
            this.tableLayoutPanel9.Controls.Add(this.txtExtraTag, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.chkExtratag, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtGrossWt, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(5, 108);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(614, 37);
            this.tableLayoutPanel9.TabIndex = 3;
            // 
            // txtExtraTag
            // 
            this.txtExtraTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtExtraTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtExtraTag.Location = new System.Drawing.Point(502, 5);
            this.txtExtraTag.Margin = new System.Windows.Forms.Padding(4);
            this.txtExtraTag.Name = "txtExtraTag";
            this.txtExtraTag.Size = new System.Drawing.Size(107, 26);
            this.txtExtraTag.TabIndex = 12;
            this.txtExtraTag.Text = "0.0";
            // 
            // chkExtratag
            // 
            this.chkExtratag.AutoSize = true;
            this.chkExtratag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkExtratag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkExtratag.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkExtratag.Location = new System.Drawing.Point(340, 5);
            this.chkExtratag.Margin = new System.Windows.Forms.Padding(4);
            this.chkExtratag.Name = "chkExtratag";
            this.chkExtratag.Size = new System.Drawing.Size(153, 27);
            this.chkExtratag.TabIndex = 11;
            this.chkExtratag.Text = "Extra Tag";
            this.chkExtratag.UseVisualStyleBackColor = true;
            this.chkExtratag.CheckedChanged += new System.EventHandler(this.chkExtratag_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(5, 1);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(206, 35);
            this.label8.TabIndex = 0;
            this.label8.Text = "Gross Wt.";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGrossWt
            // 
            this.txtGrossWt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGrossWt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtGrossWt.Location = new System.Drawing.Point(220, 5);
            this.txtGrossWt.Margin = new System.Windows.Forms.Padding(4);
            this.txtGrossWt.Name = "txtGrossWt";
            this.txtGrossWt.Size = new System.Drawing.Size(111, 26);
            this.txtGrossWt.TabIndex = 9;
            this.txtGrossWt.Text = "0.0";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel10.ColumnCount = 4;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.Controls.Add(this.label13, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtHalmarkNo, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.label31, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtRFID_Tag_Weight, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(628, 108);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(614, 37);
            this.tableLayoutPanel10.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(311, 1);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(144, 35);
            this.label13.TabIndex = 0;
            this.label13.Text = "Hall mark No.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHalmarkNo
            // 
            this.txtHalmarkNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtHalmarkNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtHalmarkNo.Location = new System.Drawing.Point(464, 5);
            this.txtHalmarkNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtHalmarkNo.Name = "txtHalmarkNo";
            this.txtHalmarkNo.Size = new System.Drawing.Size(145, 26);
            this.txtHalmarkNo.TabIndex = 10;
            this.txtHalmarkNo.Text = "0.0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(4, 1);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(146, 35);
            this.label31.TabIndex = 11;
            this.label31.Text = "RFID Tag Wgt.";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRFID_Tag_Weight
            // 
            this.txtRFID_Tag_Weight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRFID_Tag_Weight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRFID_Tag_Weight.Location = new System.Drawing.Point(157, 4);
            this.txtRFID_Tag_Weight.Name = "txtRFID_Tag_Weight";
            this.txtRFID_Tag_Weight.Size = new System.Drawing.Size(146, 26);
            this.txtRFID_Tag_Weight.TabIndex = 12;
            this.txtRFID_Tag_Weight.Text = "0.0";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel12.ColumnCount = 3;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.29F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.29F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.42F));
            this.tableLayoutPanel12.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.cmbTagType, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.btnAddTagType, 2, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(5, 196);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(614, 40);
            this.tableLayoutPanel12.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(5, 1);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(207, 38);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tag Type";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbTagType
            // 
            this.cmbTagType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbTagType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cmbTagType.FormattingEnabled = true;
            this.cmbTagType.Location = new System.Drawing.Point(221, 5);
            this.cmbTagType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbTagType.Name = "cmbTagType";
            this.cmbTagType.Size = new System.Drawing.Size(298, 28);
            this.cmbTagType.TabIndex = 13;
            this.cmbTagType.SelectedIndexChanged += new System.EventHandler(this.cmbTagType_SelectedIndexChanged);
            // 
            // btnAddTagType
            // 
            this.btnAddTagType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddTagType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAddTagType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAddTagType.Location = new System.Drawing.Point(528, 5);
            this.btnAddTagType.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddTagType.Name = "btnAddTagType";
            this.btnAddTagType.Size = new System.Drawing.Size(81, 30);
            this.btnAddTagType.TabIndex = 14;
            this.btnAddTagType.Text = "+";
            this.btnAddTagType.UseVisualStyleBackColor = true;
            this.btnAddTagType.Click += new System.EventHandler(this.btnAddTagType_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtTagNo, 1, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(628, 196);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(614, 40);
            this.tableLayoutPanel13.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label15.Location = new System.Drawing.Point(5, 1);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(297, 38);
            this.label15.TabIndex = 0;
            this.label15.Text = "Tag Number";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTagNo
            // 
            this.txtTagNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTagNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTagNo.Location = new System.Drawing.Point(311, 5);
            this.txtTagNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTagNo.Name = "txtTagNo";
            this.txtTagNo.Size = new System.Drawing.Size(298, 26);
            this.txtTagNo.TabIndex = 15;
            this.txtTagNo.TextChanged += new System.EventHandler(this.txtTagNo_TextChanged);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.32533F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.34034F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.33433F));
            this.tableLayoutPanel14.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.lblTagID, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.btnTagPrint, 2, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(5, 245);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(614, 37);
            this.tableLayoutPanel14.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(5, 1);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(207, 35);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tag ID";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTagID
            // 
            this.lblTagID.AutoSize = true;
            this.lblTagID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTagID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lblTagID.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTagID.Location = new System.Drawing.Point(221, 1);
            this.lblTagID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTagID.Name = "lblTagID";
            this.lblTagID.Size = new System.Drawing.Size(299, 35);
            this.lblTagID.TabIndex = 1;
            this.lblTagID.Text = "---";
            this.lblTagID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTagPrint
            // 
            this.btnTagPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTagPrint.Location = new System.Drawing.Point(528, 4);
            this.btnTagPrint.Name = "btnTagPrint";
            this.btnTagPrint.Size = new System.Drawing.Size(82, 29);
            this.btnTagPrint.TabIndex = 2;
            this.btnTagPrint.Text = "Print";
            this.btnTagPrint.UseVisualStyleBackColor = true;
            this.btnTagPrint.Click += new System.EventHandler(this.btnTagPrint_Click);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 3;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel15, 2);
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel15.Controls.Add(this.btnSave, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.btnWriteSave, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.btnCancel, 2, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(5, 496);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(1237, 53);
            this.tableLayoutPanel15.TabIndex = 9;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSave.Location = new System.Drawing.Point(4, 4);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(404, 45);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnWriteSave
            // 
            this.btnWriteSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnWriteSave.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnWriteSave.Location = new System.Drawing.Point(416, 4);
            this.btnWriteSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnWriteSave.Name = "btnWriteSave";
            this.btnWriteSave.Size = new System.Drawing.Size(404, 45);
            this.btnWriteSave.TabIndex = 18;
            this.btnWriteSave.Text = "Write and  Save";
            this.btnWriteSave.UseVisualStyleBackColor = true;
            this.btnWriteSave.Click += new System.EventHandler(this.btnWriteSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(828, 4);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(405, 45);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(4, 289);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(616, 200);
            this.tableLayoutPanel16.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(302, 196);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnImgClear);
            this.flowLayoutPanel1.Controls.Add(this.btnUploadImage);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(311, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(302, 194);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btnImgClear
            // 
            this.btnImgClear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnImgClear.Location = new System.Drawing.Point(3, 147);
            this.btnImgClear.Name = "btnImgClear";
            this.btnImgClear.Size = new System.Drawing.Size(275, 44);
            this.btnImgClear.TabIndex = 0;
            this.btnImgClear.Text = "Clear Image";
            this.btnImgClear.UseVisualStyleBackColor = true;
            this.btnImgClear.Click += new System.EventHandler(this.btnImgClear_Click);
            // 
            // btnUploadImage
            // 
            this.btnUploadImage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUploadImage.Location = new System.Drawing.Point(3, 105);
            this.btnUploadImage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUploadImage.Name = "btnUploadImage";
            this.btnUploadImage.Size = new System.Drawing.Size(275, 37);
            this.btnUploadImage.TabIndex = 16;
            this.btnUploadImage.Text = "Upload Image";
            this.btnUploadImage.UseVisualStyleBackColor = true;
            this.btnUploadImage.Click += new System.EventHandler(this.btnUploadImage_Click);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.56818F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.43182F));
            this.tableLayoutPanel17.Controls.Add(this.groupBox1, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(627, 289);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(616, 200);
            this.tableLayoutPanel17.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComboBox_baud);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.ComboBox_AlreadyOpenCOM);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.ComboBox_baud2);
            this.groupBox1.Controls.Add(this.btnComOpen);
            this.groupBox1.Controls.Add(this.cmbComPort);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(314, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel17.SetRowSpan(this.groupBox1, 2);
            this.groupBox1.Size = new System.Drawing.Size(299, 196);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Communication";
            // 
            // ComboBox_baud
            // 
            this.ComboBox_baud.FormattingEnabled = true;
            this.ComboBox_baud.Items.AddRange(new object[] {
            "9600bps",
            "19200bps",
            "38400bps",
            "57600bps",
            "115200bps"});
            this.ComboBox_baud.Location = new System.Drawing.Point(107, 55);
            this.ComboBox_baud.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ComboBox_baud.Name = "ComboBox_baud";
            this.ComboBox_baud.Size = new System.Drawing.Size(121, 24);
            this.ComboBox_baud.TabIndex = 2;
            this.ComboBox_baud.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(-3, 49);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 17);
            this.label12.TabIndex = 26;
            this.label12.Text = "Set Baud Rate";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(-1, 98);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "Opened COM Port:";
            this.label11.Visible = false;
            // 
            // ComboBox_AlreadyOpenCOM
            // 
            this.ComboBox_AlreadyOpenCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_AlreadyOpenCOM.FormattingEnabled = true;
            this.ComboBox_AlreadyOpenCOM.Location = new System.Drawing.Point(131, 97);
            this.ComboBox_AlreadyOpenCOM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ComboBox_AlreadyOpenCOM.Name = "ComboBox_AlreadyOpenCOM";
            this.ComboBox_AlreadyOpenCOM.Size = new System.Drawing.Size(121, 24);
            this.ComboBox_AlreadyOpenCOM.TabIndex = 4;
            this.ComboBox_AlreadyOpenCOM.Visible = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label47.Location = new System.Drawing.Point(-3, 28);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(91, 17);
            this.label47.TabIndex = 24;
            this.label47.Text = "Baud Rate 2:";
            this.label47.Visible = false;
            // 
            // ComboBox_baud2
            // 
            this.ComboBox_baud2.FormattingEnabled = true;
            this.ComboBox_baud2.Location = new System.Drawing.Point(107, 25);
            this.ComboBox_baud2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ComboBox_baud2.Name = "ComboBox_baud2";
            this.ComboBox_baud2.Size = new System.Drawing.Size(121, 24);
            this.ComboBox_baud2.TabIndex = 3;
            this.ComboBox_baud2.Visible = false;
            // 
            // btnComOpen
            // 
            this.btnComOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnComOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnComOpen.Location = new System.Drawing.Point(131, 143);
            this.btnComOpen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnComOpen.Name = "btnComOpen";
            this.btnComOpen.Size = new System.Drawing.Size(121, 34);
            this.btnComOpen.TabIndex = 1;
            this.btnComOpen.Text = "Open COM";
            this.btnComOpen.UseVisualStyleBackColor = true;
            this.btnComOpen.Click += new System.EventHandler(this.btnComOpen_Click);
            // 
            // cmbComPort
            // 
            this.cmbComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cmbComPort.FormattingEnabled = true;
            this.cmbComPort.Location = new System.Drawing.Point(3, 148);
            this.cmbComPort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbComPort.Name = "cmbComPort";
            this.cmbComPort.Size = new System.Drawing.Size(121, 28);
            this.cmbComPort.TabIndex = 0;
            this.cmbComPort.SelectedIndexChanged += new System.EventHandler(this.cmbComPort_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(1, 126);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "COM Port：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.Edit_ComAdr);
            this.groupBox2.Controls.Add(this.Edit_dmaxfre);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.Edit_dminfre);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.Edit_powerdBm);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.EPCC1G2);
            this.groupBox2.Controls.Add(this.ISO180006B);
            this.groupBox2.Controls.Add(this.Edit_scantime);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.Edit_Version);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.Edit_Type);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 2);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel17.SetRowSpan(this.groupBox2, 2);
            this.groupBox2.Size = new System.Drawing.Size(305, 196);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Reader Info";
            this.groupBox2.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label22.Location = new System.Drawing.Point(165, 53);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 17);
            this.label22.TabIndex = 25;
            this.label22.Text = "Ver.:";
            // 
            // Edit_ComAdr
            // 
            this.Edit_ComAdr.Location = new System.Drawing.Point(163, 78);
            this.Edit_ComAdr.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Edit_ComAdr.Name = "Edit_ComAdr";
            this.Edit_ComAdr.Size = new System.Drawing.Size(96, 22);
            this.Edit_ComAdr.TabIndex = 5;
            this.Edit_ComAdr.Visible = false;
            // 
            // Edit_dmaxfre
            // 
            this.Edit_dmaxfre.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Edit_dmaxfre.Location = new System.Drawing.Point(163, 23);
            this.Edit_dmaxfre.Margin = new System.Windows.Forms.Padding(4);
            this.Edit_dmaxfre.Name = "Edit_dmaxfre";
            this.Edit_dmaxfre.Size = new System.Drawing.Size(96, 22);
            this.Edit_dmaxfre.TabIndex = 24;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label21.Location = new System.Drawing.Point(165, 7);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 17);
            this.label21.TabIndex = 23;
            this.label21.Text = "Max.Fre:";
            // 
            // Edit_dminfre
            // 
            this.Edit_dminfre.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Edit_dminfre.Location = new System.Drawing.Point(57, 130);
            this.Edit_dminfre.Margin = new System.Windows.Forms.Padding(4);
            this.Edit_dminfre.Name = "Edit_dminfre";
            this.Edit_dminfre.Size = new System.Drawing.Size(112, 22);
            this.Edit_dminfre.TabIndex = 22;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label20.Location = new System.Drawing.Point(3, 130);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 17);
            this.label20.TabIndex = 3;
            this.label20.Text = "Min.Fre:";
            // 
            // Edit_powerdBm
            // 
            this.Edit_powerdBm.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Edit_powerdBm.Location = new System.Drawing.Point(45, 103);
            this.Edit_powerdBm.Margin = new System.Windows.Forms.Padding(4);
            this.Edit_powerdBm.Name = "Edit_powerdBm";
            this.Edit_powerdBm.Size = new System.Drawing.Size(112, 22);
            this.Edit_powerdBm.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label19.Location = new System.Drawing.Point(0, 103);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 17);
            this.label19.TabIndex = 20;
            this.label19.Text = "Power:";
            // 
            // EPCC1G2
            // 
            this.EPCC1G2.AutoSize = true;
            this.EPCC1G2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.EPCC1G2.Location = new System.Drawing.Point(156, 161);
            this.EPCC1G2.Margin = new System.Windows.Forms.Padding(4);
            this.EPCC1G2.Name = "EPCC1G2";
            this.EPCC1G2.Size = new System.Drawing.Size(98, 21);
            this.EPCC1G2.TabIndex = 19;
            this.EPCC1G2.Text = "EPCC1-G2";
            this.EPCC1G2.UseVisualStyleBackColor = true;
            // 
            // ISO180006B
            // 
            this.ISO180006B.AutoSize = true;
            this.ISO180006B.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ISO180006B.Location = new System.Drawing.Point(0, 161);
            this.ISO180006B.Margin = new System.Windows.Forms.Padding(4);
            this.ISO180006B.Name = "ISO180006B";
            this.ISO180006B.Size = new System.Drawing.Size(115, 21);
            this.ISO180006B.TabIndex = 18;
            this.ISO180006B.Text = "ISO18000-6B";
            this.ISO180006B.UseVisualStyleBackColor = true;
            // 
            // Edit_scantime
            // 
            this.Edit_scantime.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Edit_scantime.Location = new System.Drawing.Point(45, 78);
            this.Edit_scantime.Margin = new System.Windows.Forms.Padding(4);
            this.Edit_scantime.Name = "Edit_scantime";
            this.Edit_scantime.Size = new System.Drawing.Size(112, 22);
            this.Edit_scantime.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label18.Location = new System.Drawing.Point(3, 80);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 17);
            this.label18.TabIndex = 14;
            this.label18.Text = "Time:";
            // 
            // Edit_Version
            // 
            this.Edit_Version.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Edit_Version.Location = new System.Drawing.Point(45, 50);
            this.Edit_Version.Margin = new System.Windows.Forms.Padding(4);
            this.Edit_Version.Name = "Edit_Version";
            this.Edit_Version.Size = new System.Drawing.Size(112, 22);
            this.Edit_Version.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label17.Location = new System.Drawing.Point(7, 53);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 17);
            this.label17.TabIndex = 7;
            this.label17.Text = "Ver.:";
            // 
            // Edit_Type
            // 
            this.Edit_Type.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Edit_Type.Location = new System.Drawing.Point(45, 23);
            this.Edit_Type.Margin = new System.Windows.Forms.Padding(4);
            this.Edit_Type.Name = "Edit_Type";
            this.Edit_Type.Size = new System.Drawing.Size(112, 22);
            this.Edit_Type.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(3, 23);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 17);
            this.label16.TabIndex = 1;
            this.label16.Text = "Type:";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.lblHexCode, 1, 1);
            this.tableLayoutPanel19.Controls.Add(this.lblAsciiCode, 1, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(627, 243);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(616, 41);
            this.tableLayoutPanel19.TabIndex = 22;
            // 
            // lblHexCode
            // 
            this.lblHexCode.AutoSize = true;
            this.lblHexCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblHexCode.Location = new System.Drawing.Point(311, 20);
            this.lblHexCode.Name = "lblHexCode";
            this.lblHexCode.Size = new System.Drawing.Size(32, 17);
            this.lblHexCode.TabIndex = 21;
            this.lblHexCode.Text = "___";
            this.lblHexCode.Visible = false;
            // 
            // lblAsciiCode
            // 
            this.lblAsciiCode.AutoSize = true;
            this.lblAsciiCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblAsciiCode.Location = new System.Drawing.Point(311, 0);
            this.lblAsciiCode.Name = "lblAsciiCode";
            this.lblAsciiCode.Size = new System.Drawing.Size(40, 17);
            this.lblAsciiCode.TabIndex = 20;
            this.lblAsciiCode.Text = "____";
            this.lblAsciiCode.Visible = false;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel11.ColumnCount = 8;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel11, 2);
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.53063F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.613572F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.10085F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.519321F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.8181F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.06409F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel11.Controls.Add(this.txtKar_melting, 7, 0);
            this.tableLayoutPanel11.Controls.Add(this.label30, 6, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtKar_wastage, 5, 0);
            this.tableLayoutPanel11.Controls.Add(this.label29, 4, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtDirect_melting, 3, 0);
            this.tableLayoutPanel11.Controls.Add(this.label28, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtDirect_wastage, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(4, 153);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1239, 35);
            this.tableLayoutPanel11.TabIndex = 23;
            // 
            // txtKar_melting
            // 
            this.txtKar_melting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKar_melting.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKar_melting.Location = new System.Drawing.Point(1083, 4);
            this.txtKar_melting.Name = "txtKar_melting";
            this.txtKar_melting.Size = new System.Drawing.Size(152, 26);
            this.txtKar_melting.TabIndex = 7;
            this.txtKar_melting.Text = "0.0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(934, 1);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(142, 33);
            this.label30.TabIndex = 6;
            this.label30.Text = "Kar melting";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKar_wastage
            // 
            this.txtKar_wastage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKar_wastage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKar_wastage.Location = new System.Drawing.Point(775, 4);
            this.txtKar_wastage.Name = "txtKar_wastage";
            this.txtKar_wastage.Size = new System.Drawing.Size(152, 26);
            this.txtKar_wastage.TabIndex = 5;
            this.txtKar_wastage.Text = "0.0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(620, 1);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(148, 33);
            this.label29.TabIndex = 4;
            this.label29.Text = "Kar wastage";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDirect_melting
            // 
            this.txtDirect_melting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDirect_melting.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirect_melting.Location = new System.Drawing.Point(502, 4);
            this.txtDirect_melting.Name = "txtDirect_melting";
            this.txtDirect_melting.Size = new System.Drawing.Size(111, 26);
            this.txtDirect_melting.TabIndex = 3;
            this.txtDirect_melting.Text = "0.0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(340, 1);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(155, 33);
            this.label28.TabIndex = 2;
            this.label28.Text = "Direct melting";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 1);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(210, 33);
            this.label27.TabIndex = 0;
            this.label27.Text = "Direct wastage ";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDirect_wastage
            // 
            this.txtDirect_wastage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDirect_wastage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirect_wastage.Location = new System.Drawing.Point(221, 4);
            this.txtDirect_wastage.Name = "txtDirect_wastage";
            this.txtDirect_wastage.Size = new System.Drawing.Size(112, 26);
            this.txtDirect_wastage.TabIndex = 1;
            this.txtDirect_wastage.Text = "0.0";
            // 
            // Write_tag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1247, 554);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Write_tag";
            this.Text = "Write_tag";
            this.Load += new System.EventHandler(this.Write_tag_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbItemType;
        private System.Windows.Forms.Button btnAddItmType;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtStone1;
        private System.Windows.Forms.RadioButton rbtStone2;
        private System.Windows.Forms.RadioButton rbtStone3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStone_details_R;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtStone_RVal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStone_details_G;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtStone_GVal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtStone_details_W;
        private System.Windows.Forms.TextBox txtStone_WVal;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtStone_details_K;
        private System.Windows.Forms.TextBox txtStone_KVal;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtGrossWt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtHalmarkNo;
        private System.Windows.Forms.CheckBox chkExtratag;
        private System.Windows.Forms.TextBox txtExtraTag;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbTagType;
        private System.Windows.Forms.Button btnAddTagType;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTagNo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTagID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnWriteSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btnUploadImage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBox_baud;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ComboBox_AlreadyOpenCOM;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox ComboBox_baud2;
        private System.Windows.Forms.Button btnComOpen;
        private System.Windows.Forms.ComboBox cmbComPort;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Edit_ComAdr;
        private System.Windows.Forms.TextBox Edit_dmaxfre;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Edit_dminfre;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Edit_powerdBm;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox EPCC1G2;
        private System.Windows.Forms.CheckBox ISO180006B;
        private System.Windows.Forms.TextBox Edit_scantime;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Edit_Version;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Edit_Type;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Label lblHexCode;
        private System.Windows.Forms.Label lblAsciiCode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtKar_melting;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtKar_wastage;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtDirect_melting;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtDirect_wastage;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtRFID_Tag_Weight;
        private System.Windows.Forms.CheckBox chk_Actual_Stone;
        private Button btnTagPrint;
        private RadioButton rbtStone0;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button btnImgClear;
    }
}

