﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace e_Tag_Write
{
    public partial class MstTagType : Form
    {
        private SqlDataAdapter adp;
        private SqlCommand cmd;
     
        private SqlConnection con = new SqlConnection();
        private string Constr = ConfigurationManager.ConnectionStrings["con"].ToString();
       
        private bool ErrFlag;
     
        private string SSQL = "";
     
        public MstTagType()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtTagType.Text = "";
            hiddenTagTypeCode.Text = "";
            btnSave.Text = "Save";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtTagType.Text == "")
            {
                ErrFlag = true;
                MessageBox.Show("Please Enter the Tag type");
                txtTagType.Focus();
            }
            else if (!ErrFlag)
            {
                if (btnSave.Text == "Save")
                {
                    SSQL = "";
                    SSQL = "Select * from MstTagType where TagTypeName='" + txtTagType.Text + "'";
                    DataTable dataTable = new DataTable();
                    con.Open();
                    cmd = new SqlCommand(SSQL, con);
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(dataTable);
                    con.Close();
                    if (dataTable.Rows.Count > 0)
                    {
                        MessageBox.Show("This Item Already Present...");
                        ErrFlag = true;
                    }
                    if (!ErrFlag)
                    {
                        SSQL = "";
                        SSQL = "insert into MstTagType(TagTypeName)";
                        SSQL = SSQL + " values('" + txtTagType.Text + "')";
                        con.Open();
                        cmd = new SqlCommand(SSQL, con);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        btnClear_Click(null, null);
                        Laod_Data();
                    }
                }
                else
                {
                    SSQL = "";
                    SSQL = "update  MstTagType SET TagTypeName=";
                    string[] textArray1 = new string[] { SSQL, " '", txtTagType.Text, "' where TagTypeCode='", hiddenTagTypeCode.Text, "'" };
                    SSQL = string.Concat(textArray1);
                    con.Open();
                    cmd = new SqlCommand(SSQL, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    btnClear_Click(null, null);
                    Laod_Data();
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
            if (e.ColumnIndex == 2)
            {
                DataTable dataTable = new DataTable();
                SSQL = "";
                SSQL = "Select TagTypeCode,TagTypeName from MstTagType where TagTypeCode='" + row.Cells[0].Value.ToString() + "'";
                con.Open();
                cmd = new SqlCommand(SSQL, con);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dataTable);
                con.Close();
                if (dataTable.Rows.Count > 0)
                {
                    hiddenTagTypeCode.Text = dataTable.Rows[0]["TagTypeCode"].ToString();
                    txtTagType.Text = dataTable.Rows[0]["TagTypeName"].ToString();
                    //btnSave.Text = "Update";
                }
            }
            if (e.ColumnIndex == 3)
            {
                switch (MessageBox.Show("Are you sure want to delete this details ?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk))
                {
                    case DialogResult.Yes:
                        SSQL = "";
                        SSQL = "Delete from MstTagType where TagTypeCode='" + row.Cells[0].Value.ToString() + "'";
                        con.Open();
                        cmd = new SqlCommand(SSQL, con);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        Laod_Data();
                        return;

                    case DialogResult.No:
                        Laod_Data();
                        break;
                }
            }
        }
        private void Laod_Data()
        {
            DataTable dataTable = new DataTable();
            SSQL = "";
            SSQL = "Select * from MstTagType";
            con.Open();
            cmd = new SqlCommand(SSQL, con);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(dataTable);
            con.Close();
            dataGridView1.DataSource = dataTable;
        }

        private void MstTagType_Load(object sender, EventArgs e)
        {
            con.ConnectionString = Constr;
            Laod_Data();
        }
    }
}
