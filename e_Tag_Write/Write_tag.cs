﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _2CQR;
using ReaderB;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;

namespace e_Tag_Write
{
    public partial class Write_tag : Form
    {
        private SqlDataAdapter adp;
        private int CardNum1;
        private SqlCommand cmd;
        private bool ComOpen;
        private SqlConnection con = new SqlConnection();
        private string Constr = ConfigurationManager.ConnectionStrings["con"].ToString();
        private string Edit_CmdComAddr = "FF";
        private bool ErrFlag;
        private bool fAppClosed;
        private byte fBaud;
        private int fCmdRet = 30;
        private byte fComAdr = 0xff;
        private double fdmaxfre;
        private double fdminfre;
        private int ferrorcode;
        private string fInventory_EPC_List;
        private bool fIsInventoryScan;
        private bool fisinventoryscan_6B;
        private int fOpenComIndex;
        private byte[] fOperEPC = new byte[0x24];
        private byte[] fOperID_6B = new byte[8];
        private byte[] fPassWord = new byte[4];
        private int frmcomportindex;
        private bool fTimer_6B_ReadWrite;
        private ArrayList list = new ArrayList();
        private byte Maskadr;
        private byte MaskFlag;
        private byte MaskLen;
        private OpenFileDialog opnfd_img = new OpenFileDialog();
        private string SSQL = "";
        public DataTable dt_printdata;
        public Write_tag()
        {
            InitializeComponent();
        }
        private void AddCmdLog(string CMD, string cmdStr, int cmdRet)
        {
        }
        private void AddCmdLog(string CMD, string cmdStr, int cmdRet, int errocode)
        {
        }
        private void cmbItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnImgClear_Click(sender, e);
        }
        private void cmbTagType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTagType.SelectedIndex != 0)
            {

                DataTable dataTable = new DataTable();
                SSQL = "";
                SSQL = "Select Isnull(Max(TagNo)+1,1) as Max from TagNoMst where TagTypeID='" + ((cmbTagType.SelectedValue == null) ? null : cmbTagType.SelectedValue.ToString()) + "'";
                con.Open();
                cmd = new SqlCommand(SSQL, con);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dataTable);
                con.Close();
                if (dataTable.Rows.Count > 0)
                {
                    txtTagNo.Text = dataTable.Rows[0][0].ToString();
                }
                txtTagNo_TextChanged(sender, e);
            }
        }
        private void txtTagNo_TextChanged(object sender, EventArgs e)
        {
            int num;
            lblTagID.Text = "";
            lblTagID.Text = cmbTagType.Text + "-" + txtTagNo.Text;
            lblAsciiCode.Text = "";
            byte[] bytes = Encoding.ASCII.GetBytes(lblTagID.Text);
            for (num = 0; num < bytes.Length; num++)
            {
                lblAsciiCode.Text = lblAsciiCode.Text + ((int)bytes[num]).ToString();
            }
            lblHexCode.Text = "";
            string str = "";
            char[] chArray = lblTagID.Text.ToCharArray();
            for (num = 0; num < chArray.Length; num++)
            {
                int num3 = Convert.ToInt32(chArray[num]);
                str = str + string.Format("{0:X}", num3);
            }
            for (int i = str.Length; i < 0x18; i++)
            {
                str = str + "0";
            }
            lblHexCode.Text = str.ToString();
        }
        private void btnAddItmType_Click(object sender, EventArgs e)
        {
            MstItemType type = new MstItemType();
            type.ShowDialog();
            if ((type.DialogResult == DialogResult.OK) || (type.DialogResult == DialogResult.Cancel))
            {
                Load_Itemtype();
            }
        }
        private void btnAddTagType_Click(object sender, EventArgs e)
        {
            MstTagType type = new MstTagType();
            type.ShowDialog();
            if ((type.DialogResult == DialogResult.OK) || (type.DialogResult == DialogResult.Cancel))
            {
                Load_TagType();
            }
        }
        private void API_CALL()
        {
            string[] tempContent = new string[30];
            tempContent[0] = cmbItemType.Text;
            string str = "0";
            decimal num = new decimal();
            decimal num2 = new decimal();
            decimal num3 = new decimal();
            decimal num4 = new decimal();
            string str2 = "No";
            string str_actual_Stone = "";
            if (rbtStone1.Checked)
            {
                str = "1";
            }
            if (rbtStone2.Checked)
            {
                str = "2";
            }
            if (rbtStone3.Checked)
            {
                str = "3";
            }
            if (chkExtratag.Checked)
            {
                str2 = "Yes";
            }
            if (chk_Actual_Stone.Checked)
            {
                str_actual_Stone = "2";
            }
            else
            {
                str_actual_Stone = "1";
            }
            tempContent[1] = str;
            num = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_R.Text) * Convert.ToDecimal(txtStone_RVal.Text))), 2, MidpointRounding.AwayFromZero);
            num2 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_G.Text) * Convert.ToDecimal(txtStone_GVal.Text))), 2, MidpointRounding.AwayFromZero);
            num3 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_W.Text) * Convert.ToDecimal(txtStone_WVal.Text))), 2, MidpointRounding.AwayFromZero);
            num4 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_K.Text) * Convert.ToDecimal(txtStone_KVal.Text))), 2, MidpointRounding.AwayFromZero);
            tempContent[2] = num.ToString();
            tempContent[3] = num2.ToString();
            tempContent[4] = num3.ToString();
            tempContent[5] = num4.ToString();
            tempContent[6] = txtHalmarkNo.Text.ToString();
            tempContent[7] = str2.ToString();
            tempContent[8] = txtExtraTag.Text.ToString();
            tempContent[9] = lblTagID.Text.ToString();
            tempContent[10] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").ToString();
            tempContent[11] = cmbTagType.Text.ToString();
            tempContent[12] = imgBase64((opnfd_img != null) ? opnfd_img.FileName.ToString() : "").ToString();
            tempContent[13] = (((num + num2) + num3) + num4).ToString();
            tempContent[14] = txtGrossWt.Text.ToString();
            tempContent[15] = ((decimal)(Convert.ToDecimal(txtGrossWt.Text) - Convert.ToDecimal(txtExtraTag.Text))).ToString("0.000");
            tempContent[16] = txtRFID_Tag_Weight.Text.ToString();
            tempContent[17] = txtKar_wastage.Text.ToString();
            tempContent[18] = txtKar_melting.Text.ToString();
            tempContent[19] = txtDirect_wastage.Text.ToString();
            tempContent[20] = txtDirect_melting.Text.ToString();
            tempContent[21] = str_actual_Stone.ToString();
            string str3 = "";
            if (new SelvaWebService().update(tempContent))
            {
                str3 = "1";
            }
            else
            {
                str3 = "2";
            }
            SSQL = "";
            string[] textArray1 = new string[] { "Update e_Tag_Write set API_Status='", str3, "' where Tag_ID='", lblTagID.Text, "'" };
            SSQL = string.Concat(textArray1);
            con.Open();
            cmd = new SqlCommand(SSQL, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            cmbTagType.ResetText();
            cmbItemType.ResetText();
            chkExtratag.Checked = false;
            pictureBox1.Image = null;
        }
        private void btnComOpen_Click(object sender, EventArgs e)
        {
            string str;
            int port = 0;
            int num2 = 30;
            Cursor = Cursors.WaitCursor;
            if (ComOpen)
            {
                if (ComOpen)
                {
                    try
                    {
                        if (ComboBox_AlreadyOpenCOM.SelectedIndex < 0)
                        {
                            MessageBox.Show("Please Choose COM Port to close", "Information");
                        }
                        else
                        {
                            str = ComboBox_AlreadyOpenCOM.SelectedItem.ToString();
                            port = Convert.ToInt32(str.Substring(3, str.Length - 3));
                            fCmdRet = StaticClassReaderB.CloseSpecComPort(port);
                            if (fCmdRet == 0)
                            {
                                ComboBox_AlreadyOpenCOM.Items.RemoveAt(0);
                                if (ComboBox_AlreadyOpenCOM.Items.Count != 0)
                                {
                                    str = ComboBox_AlreadyOpenCOM.SelectedItem.ToString();
                                    port = Convert.ToInt32(str.Substring(3, str.Length - 3));
                                    StaticClassReaderB.CloseSpecComPort(port);
                                    fComAdr = 0xff;
                                    StaticClassReaderB.OpenComPort(port, ref fComAdr, fBaud, ref frmcomportindex);
                                    fOpenComIndex = frmcomportindex;
                                    Button3_Click(sender, e);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Serial Communication Error", "Information");
                            }
                        }
                    }
                    finally
                    {
                        Cursor = Cursors.Default;
                    }
                    if (ComboBox_AlreadyOpenCOM.Items.Count != 0)
                    {
                        ComboBox_AlreadyOpenCOM.SelectedIndex = 0;
                        return;
                    }
                    fOpenComIndex = -1;
                    ComboBox_AlreadyOpenCOM.Items.Clear();
                    ComboBox_AlreadyOpenCOM.Refresh();
                    ComOpen = false;
                    btnComOpen.Text = "Open COM";
                }
                return;
            }
            if (Edit_CmdComAddr == "")
            {
                Edit_CmdComAddr = "FF";
            }
            fComAdr = Convert.ToByte(Edit_CmdComAddr, 0x10);
            try
            {
                if (cmbComPort.SelectedIndex == 0)
                {
                    fBaud = Convert.ToByte(ComboBox_baud2.SelectedIndex);
                    if (fBaud > 2)
                    {
                        fBaud = Convert.ToByte((int)(fBaud + 2));
                    }
                    num2 = StaticClassReaderB.AutoOpenComPort(ref port, ref fComAdr, fBaud, ref frmcomportindex);
                    fOpenComIndex = frmcomportindex;
                    if (num2 == 0)
                    {
                        ComOpen = true;
                        btnComOpen.Text = "Close Port";
                        Button3_Click(sender, e);
                        if (fBaud > 3)
                        {
                            ComboBox_baud.SelectedIndex = Convert.ToInt32((int)(fBaud - 2));
                        }
                        else
                        {
                            ComboBox_baud.SelectedIndex = Convert.ToInt32(fBaud);
                        }
                        Button3_Click(sender, e);
                        if ((fCmdRet == 0x35) | (fCmdRet == 0x30))
                        {
                            MessageBox.Show("Serial Communication Error or Occupied", "Information");
                            StaticClassReaderB.CloseSpecComPort(frmcomportindex);
                            ComOpen = false;
                        }
                    }
                }
                else
                {
                    str = cmbComPort.SelectedItem.ToString().Trim();
                    port = Convert.ToInt32(str.Substring(3, str.Length - 3));
                    for (int i = 6; i >= 0; i--)
                    {
                        fBaud = Convert.ToByte(i);
                        if (fBaud != 3)
                        {
                            num2 = StaticClassReaderB.OpenComPort(port, ref fComAdr, fBaud, ref frmcomportindex);
                            fOpenComIndex = frmcomportindex;
                            switch (num2)
                            {
                                case 0x35:
                                    MessageBox.Show("COM Opened", "Information");
                                    return;

                                case 0:
                                    ComOpen = true;
                                    btnComOpen.Text = "Close Port";
                                    if (fBaud > 3)
                                    {
                                        ComboBox_baud.SelectedIndex = Convert.ToInt32((int)(fBaud - 2));
                                    }
                                    else
                                    {
                                        ComboBox_baud.SelectedIndex = Convert.ToInt32(fBaud);
                                    }
                                    if ((fCmdRet != 0x35) && (fCmdRet != 0x30))
                                    {
                                        goto Label_0291;
                                    }
                                    ComOpen = false;
                                    MessageBox.Show("Serial Communication Error or Occupied", "Information");
                                    StaticClassReaderB.CloseSpecComPort(frmcomportindex);
                                    return;
                            }
                        }
                    }
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        Label_0291:
            if (((fOpenComIndex != -1) & (num2 != 0x35)) & (num2 != 0x30))
            {
                ComboBox_AlreadyOpenCOM.Items.Add("COM" + Convert.ToString(fOpenComIndex));
                ComboBox_AlreadyOpenCOM.SelectedIndex++;
                ComOpen = true;
                btnComOpen.Text = "Close Port";
            }
            if ((fOpenComIndex == -1) && (num2 == 0x30))
            {
                MessageBox.Show("Serial Communication Error", "Information");
            }
            if (((((ComboBox_AlreadyOpenCOM.Items.Count > 0) & (fOpenComIndex != -1)) & (num2 != 0x35)) & (num2 != 0x30)) & (fCmdRet == 0))
            {
                fComAdr = Convert.ToByte(Edit_ComAdr.Text, 0x10);
                str = ComboBox_AlreadyOpenCOM.SelectedItem.ToString();
                frmcomportindex = Convert.ToInt32(str.Substring(3, str.Length - 3));
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            string str = "";
            string str2 = "";
            string str3 = "0";
            string str_actual_Stone = "";
            ErrFlag = false;
            if (!ComOpen)
            {
                ErrFlag = true;
                MessageBox.Show("Please Connect the Device and Try Again", "Information");
            }
            else
            {
                try
                {
                    ErrFlag = false;
                    if (cmbItemType.Text == "-Select-")
                    {
                        MessageBox.Show("Please Select the Item Type");
                        ErrFlag = true;
                    }
                    else if (cmbTagType.Text == "-Select-")
                    {
                        MessageBox.Show("Please Select the Tag Type");
                        ErrFlag = true;
                    }
                    else
                    {
                        if (txtStone_details_R.Text == "")
                        {
                            txtStone_details_R.Text = "0.0";
                        }
                        if (txtStone_RVal.Text == "")
                        {
                            txtStone_RVal.Text = "0.0";
                        }
                        if (txtStone_details_G.Text == "")
                        {
                            txtStone_details_G.Text = "0.0";
                        }
                        if (txtStone_GVal.Text == "")
                        {
                            txtStone_GVal.Text = "0.0";
                        }
                        if (txtStone_details_W.Text == "")
                        {
                            txtStone_details_W.Text = "0.0";
                        }
                        if (txtStone_WVal.Text == "")
                        {
                            txtStone_WVal.Text = "0.0";
                        }
                        if (txtStone_details_K.Text == "")
                        {
                            txtStone_details_K.Text = "0.0";
                        }
                        if (txtStone_KVal.Text == "")
                        {
                            txtStone_KVal.Text = "0.0";
                        }
                        if (txtDirect_wastage.Text == "")
                        {
                            txtDirect_wastage.Text = "0.0";
                        }
                        if (txtDirect_melting.Text == "")
                        {
                            txtDirect_melting.Text = "0.0";
                        }
                        if (txtKar_wastage.Text == "")
                        {
                            txtKar_wastage.Text = "0.0";
                        }
                        if (txtKar_melting.Text == "")
                        {
                            txtKar_melting.Text = "0.0";
                        }
                        if (txtRFID_Tag_Weight.Text == "")
                        {
                            txtRFID_Tag_Weight.Text = "0.0";
                        }
                        if (txtGrossWt.Text == "")
                        {
                            MessageBox.Show("Please fill Gross Weight");
                            ErrFlag = true;
                            return;
                        }
                        else
                        {
                            if (chkExtratag.Checked)
                            {
                                if (txtExtraTag.Text == "" && txtExtraTag.Text == "0.0")
                                {
                                    txtExtraTag.Text = "0.0";
                                    MessageBox.Show("Please fill Extra Tag Weight");
                                    ErrFlag = true;
                                    return;
                                }
                                str3 = ((decimal)(Convert.ToDecimal(txtGrossWt.Text) - Convert.ToDecimal(txtExtraTag.Text))).ToString("0.000");
                            }
                            else
                            {
                                str3 = Convert.ToDecimal(txtGrossWt.Text).ToString("0.000");
                            }
                            if (!chkExtratag.Checked)
                            {
                                txtExtraTag.Text = "0.0";
                            }

                            if (txtTagNo.Text == "")
                            {
                                cmbTagType_SelectedIndexChanged(null, null);
                            }
                            if (!ErrFlag)
                            {
                                string str4 = "0";
                                decimal num2 = new decimal();
                                decimal num3 = new decimal();
                                decimal num4 = new decimal();
                                decimal num5 = new decimal();
                                string str5 = "No";
                                if (rbtStone1.Checked)
                                {
                                    str4 = "1";
                                }
                                if (rbtStone2.Checked)
                                {
                                    str4 = "2";
                                }
                                if (rbtStone3.Checked)
                                {
                                    str4 = "3";
                                }
                                if (chkExtratag.Checked)
                                {
                                    str5 = "Yes";
                                }
                                if (chk_Actual_Stone.Checked)
                                {
                                    str_actual_Stone = "2";
                                }
                                else
                                {
                                    str_actual_Stone = "1";
                                }
                                num2 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_R.Text) * Convert.ToDecimal(txtStone_RVal.Text))), 2, MidpointRounding.AwayFromZero);
                                num3 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_G.Text) * Convert.ToDecimal(txtStone_GVal.Text))), 2, MidpointRounding.AwayFromZero);
                                num4 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_W.Text) * Convert.ToDecimal(txtStone_WVal.Text))), 2, MidpointRounding.AwayFromZero);
                                num5 = Math.Round(Convert.ToDecimal((decimal)(Convert.ToDecimal(txtStone_details_K.Text) * Convert.ToDecimal(txtStone_KVal.Text))), 2, MidpointRounding.AwayFromZero);
                                DataTable dataTable = new DataTable();
                                SSQL = "";
                                SSQL = "Select Auto_ID from e_Tag_Write where Tag_ID='" + lblTagID.Text + "' and TagWrite_Status='1'";
                                if (con.State == ConnectionState.Open)
                                {
                                    con.Close();
                                }
                                con.Open();
                                cmd = new SqlCommand(SSQL, con);
                                adp = new SqlDataAdapter(cmd);
                                adp.Fill(dataTable);
                                con.Close();
                                if (dataTable.Rows.Count > 0)
                                {
                                    ErrFlag = true;
                                    MessageBox.Show("This Tag Already Saved!!!");
                                }
                                else
                                {
                                    str = Write_Tag_Data(lblHexCode.Text);
                                    if (str != "2")
                                    {
                                        SSQL = "";
                                        SSQL = " delete from e_Tag_Write where Tag_ID='" + lblTagID.Text + "'";
                                        if (con.State == ConnectionState.Open)
                                        {
                                            con.Close();
                                        }
                                        con.Open();
                                        cmd = new SqlCommand(SSQL, con);
                                        cmd.ExecuteNonQuery();
                                        con.Close();
                                        SSQL = "";
                                        SSQL = "insert into e_Tag_Write(ItemTypeCode,ItemTypeName,No_of_Stones,Stone_Details_R,Stone_Details_G,Stone_Detials_W,Stone_Details_K,Gross_Wt,";
                                        SSQL = SSQL + " Halmark_no,IsExtra_Tag,Extra_tag,TagTypeCode,TagTypeName,Tag_ID,Tag_No,_Tag_no,Date_Time,HexCode,AsciiCode,TagWrite_Status,API_Status,NetWt,Direct_Wastage,Direct_Melting,Kar_Wastage,Kar_Melting,RFID_Tag_Weight,actual_stone)";
                                        string[] textArray1 = new string[] { SSQL, " values('", (cmbItemType.SelectedValue == null) ? null : cmbItemType.SelectedValue.ToString(), "','", cmbItemType.Text, "','", str4, "','", num2.ToString(), "'" };
                                        SSQL = string.Concat(textArray1);
                                        string[] textArray2 = new string[] { SSQL, ", '", num3.ToString(), "','", num4.ToString(), "','", num5.ToString(), "','", txtGrossWt.Text, "'" };
                                        SSQL = string.Concat(textArray2);
                                        string[] textArray3 = new string[] { SSQL, ",'", txtHalmarkNo.Text, "','", str5, "','", txtExtraTag.Text, "','", (cmbTagType.SelectedValue == null) ? null : cmbTagType.SelectedValue.ToString(), "','", cmbTagType.Text, "'" };
                                        SSQL = string.Concat(textArray3);
                                        string[] textArray4 = new string[] {
                                            SSQL, ",'", lblTagID.Text, "','", txtTagNo.Text, "','", lblHexCode.Text, "',getdate(),'", lblHexCode.Text, "','", lblAsciiCode.Text, "','", str, "','", str2, "','",
                                            str3, "','",txtDirect_melting.Text,"','",txtDirect_wastage.Text,"','",txtKar_melting.Text,"','",txtKar_wastage.Text,"','",txtRFID_Tag_Weight.Text,"','",str_actual_Stone,"');"
                                         };
                                        SSQL = string.Concat(textArray4);
                                        if (con.State == ConnectionState.Open)
                                        {
                                            con.Close();
                                        }
                                        con.Open();
                                        cmd = new SqlCommand(SSQL, con);
                                        cmd.ExecuteNonQuery();
                                        con.Close();
                                        SSQL = " delete from TagNoMst where TagNo='" + txtTagNo.Text + "'";
                                        SSQL = " Insert into TagNoMst(TagTypeID,TagNo)";
                                        string[] textArray5 = new string[] { SSQL, " values('", (cmbTagType.SelectedValue == null) ? null : cmbTagType.SelectedValue.ToString(), "','", txtTagNo.Text, "')" };
                                        SSQL = string.Concat(textArray5);
                                        con.Open();
                                        cmd = new SqlCommand(SSQL, con);
                                        cmd.ExecuteNonQuery();
                                        con.Close();
                                        Write_tag _tag1 = new Write_tag
                                        {
                                            Enabled = false
                                        };
                                        //  btnPrint_Barcode(sender, e);
                                        API_CALL();
                                        _tag1.Enabled = true;

                                        MessageBox.Show("Data Saved Successfully", "Information");

                                        if (dt_printdata == null)
                                        {
                                            dt_printdata = new DataTable();
                                            dt_printdata.Columns.Add("print1");
                                            dt_printdata.Columns.Add("print2");
                                            dt_printdata.Columns.Add("print3");
                                            DataRow dr;
                                            dr = dt_printdata.NewRow();
                                            dr["print1"] = "N.Wt : ";
                                            dr["print2"] = str3;
                                            dr["print3"] = lblTagID.Text;
                                            dt_printdata.Rows.Add(dr);
                                        }
                                        else
                                        {
                                            DataRow dr;
                                            dr = dt_printdata.NewRow();
                                            dr["print1"] = "N.Wt :";
                                            dr["print2"] = str3;
                                            dr["print3"] = lblTagID.Text;
                                            dt_printdata.Rows.Add(dr);

                                            if (dt_printdata.Rows.Count == 3)
                                            {
                                                btnPrint_Sticker();
                                                dt_printdata = null;
                                            }
                                        }
                                        cmbTagType_SelectedIndexChanged(sender, e);
                                        txtGrossWt.Text = "";
                                        txtGrossWt.Focus();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception EX)
                {
                    MessageBox.Show("Data Save Error:" + EX, "Error");
                }
            }
        }
        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            opnfd_img.Filter = "Image Files (*.jpg;*.jpeg;.*.gif;)|*.jpg;*.jpeg;.*.gif";
            if (opnfd_img.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = new Bitmap(opnfd_img.FileName);
            }
        }
        private void btnWriteSave_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            byte[] trType = new byte[2];
            byte[] versionInfo = new byte[2];
            byte readerType = 0;
            byte scanTime = 0;
            byte dmaxfre = 0;
            byte dminfre = 0;
            byte powerdBm = 0;
            Edit_Version.Text = "";
            Edit_ComAdr.Text = "";
            Edit_scantime.Text = "";
            Edit_Type.Text = "";
            ISO180006B.Checked = false;
            EPCC1G2.Checked = false;
            Edit_powerdBm.Text = "";
            Edit_dminfre.Text = "";
            Edit_dmaxfre.Text = "";
            fCmdRet = StaticClassReaderB.GetReaderInformation(ref fComAdr, versionInfo, ref readerType, trType, ref dmaxfre, ref dminfre, ref powerdBm, ref scanTime, frmcomportindex);
            if (fCmdRet == 0)
            {
                Edit_Version.Text = Convert.ToString(versionInfo[0], 10).PadLeft(2, '0') + "." + Convert.ToString(versionInfo[1], 10).PadLeft(2, '0');
                Edit_ComAdr.Text = Convert.ToString(fComAdr, 0x10).PadLeft(2, '0');
                Edit_scantime.Text = Convert.ToString(scanTime, 10).PadLeft(2, '0') + "*100ms";
                Edit_powerdBm.Text = Convert.ToString(powerdBm, 10).PadLeft(2, '0');
                switch (Convert.ToByte((int)(((dmaxfre & 0xc0) >> 4) | (dminfre >> 6))))
                {
                    case 0:
                        fdminfre = 902.6 + ((dminfre & 0x3f) * 0.4);
                        fdmaxfre = 902.6 + ((dmaxfre & 0x3f) * 0.4);
                        break;

                    case 1:
                        fdminfre = 920.125 + ((dminfre & 0x3f) * 0.25);
                        fdmaxfre = 920.125 + ((dmaxfre & 0x3f) * 0.25);
                        break;

                    case 2:
                        fdminfre = 902.75 + ((dminfre & 0x3f) * 0.5);
                        fdmaxfre = 902.75 + ((dmaxfre & 0x3f) * 0.5);
                        break;

                    case 3:
                        fdminfre = 917.1 + ((dminfre & 0x3f) * 0.2);
                        fdmaxfre = 917.1 + ((dmaxfre & 0x3f) * 0.2);
                        break;

                    case 4:
                        fdminfre = 865.1 + ((dminfre & 0x3f) * 0.2);
                        fdmaxfre = 865.1 + ((dmaxfre & 0x3f) * 0.2);
                        break;
                }
                Edit_dminfre.Text = Convert.ToString(fdminfre) + "MHz";
                Edit_dmaxfre.Text = Convert.ToString(fdmaxfre) + "MHz";
                Edit_Type.Text = "UHFReader09";
                if ((trType[0] & 2) == 2)
                {
                    ISO180006B.Checked = true;
                    EPCC1G2.Checked = true;
                }
                else
                {
                    ISO180006B.Checked = false;
                    EPCC1G2.Checked = false;
                }
            }
        }
        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder builder = new StringBuilder(data.Length * 3);
            foreach (byte num2 in data)
            {
                builder.Append(Convert.ToString(num2, 0x10).PadLeft(2, '0'));
            }
            return builder.ToString().ToUpper();
        }
        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
            {
                buffer[i / 2] = Convert.ToByte(s.Substring(i, 2), 0x10);
            }
            return buffer;
        }
        private object imgBase64(string image)
        {
            string str = "";
            if (image != "")
            {
                str = Convert.ToBase64String(File.ReadAllBytes(image));
            }
            return str;
        }
        private void chkExtratag_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExtratag.Checked)
            {
                txtExtraTag.Visible = true;
            }
            else
            {
                txtExtraTag.Visible = false;
            }
        }
        private void cmbComPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox_baud2.Items.Clear();
            if (cmbComPort.SelectedIndex == 0)
            {
                ComboBox_baud2.Items.Add("9600bps");
                ComboBox_baud2.Items.Add("19200bps");
                ComboBox_baud2.Items.Add("38400bps");
                ComboBox_baud2.Items.Add("57600bps");
                ComboBox_baud2.Items.Add("115200bps");
                ComboBox_baud2.SelectedIndex = 3;
            }
            else
            {
                ComboBox_baud2.Items.Add("Auto");
                ComboBox_baud2.SelectedIndex = 0;
            }
        }
        private void InitComList()
        {
            int num = 0;
            cmbComPort.Items.Clear();
            cmbComPort.Items.Add(" AUTO");
            for (num = 1; num < 13; num++)
            {
                cmbComPort.Items.Add(" COM" + Convert.ToString(num));
            }
            cmbComPort.SelectedIndex = 0;
        }
        private void Load_Itemtype()
        {
            DataTable dataTable = new DataTable();
            SSQL = "";
            SSQL = "Select * from MstItemType";
            con.Open();
            cmd = new SqlCommand(SSQL, con);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(dataTable);
            con.Close();
            DataRow row = dataTable.NewRow();
            row[0] = 0;
            row[1] = "-Select-";
            dataTable.Rows.InsertAt(row, 0);
            dataTable.AcceptChanges();
            cmbItemType.DataSource = dataTable;
            cmbItemType.DisplayMember = "ItemType";
            cmbItemType.ValueMember = "ItemTypeCode";
        }
        private void Load_TagType()
        {
            DataTable dataTable = new DataTable();
            SSQL = "";
            SSQL = "Select * from MstTagType";
            con.Open();
            cmd = new SqlCommand(SSQL, con);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(dataTable);
            con.Close();
            DataRow row = dataTable.NewRow();
            row[0] = 0;
            row[1] = "-Select-";
            dataTable.Rows.InsertAt(row, 0);
            dataTable.AcceptChanges();
            cmbTagType.DataSource = dataTable;
            cmbTagType.DisplayMember = "TagTypeName";
            cmbTagType.ValueMember = "TagTypeCode";
        }
        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            using (FrmImageView view = new FrmImageView())
            {
                Bitmap image = (Bitmap)pictureBox1.Image;
                view.StartPosition = FormStartPosition.CenterScreen;
                view.Size = image.Size;
                view.Text = "image";
                PictureBox box = new PictureBox
                {
                    Dock = DockStyle.Fill,
                    Image = image
                };
                view.Controls.Add(box);
                view.ShowDialog();
            }
        }
        private string Write_Tag_Data(string text)
        {
            byte[] buffer1 = new byte[100];
            string str = "";
            if ((text.Length % 4) != 0)
            {
                MessageBox.Show("Please input EPC Data in words in hexadecimal form!", "Information");
                str = "2";
            }
            byte writeEPClen = Convert.ToByte((int)(text.Length / 2));
            byte[] writeEPC = new byte[Convert.ToByte((int)(text.Length / 4))];
            writeEPC = HexStringToByteArray(text);
            fPassWord = HexStringToByteArray("00000000");
            fCmdRet = StaticClassReaderB.WriteEPC_G2(ref fComAdr, fPassWord, writeEPC, writeEPClen, ref ferrorcode, frmcomportindex);
            if (fCmdRet == 0)
            {
                MessageBox.Show("Write EPC successfully");
                return "1";
            }
            return "2";
        }
        private void Write_tag_Load(object sender, EventArgs e)
        {
            con.ConnectionString = Constr;
            InitComList();
            Load_Itemtype();
            Load_TagType();
            chkExtratag_CheckedChanged(sender, e);
        }
        private void btnPrint_Sticker()
        {
            //string WT1 = "TSC Printers";
            //string B1 = "20080101";
            //byte[] result_unicode = System.Text.Encoding.GetEncoding("utf-16").GetBytes("unicode test");
            //byte[] result_utf8 = System.Text.Encoding.UTF8.GetBytes("TEXT 40,620,\"ARIAL.TTF\",0,12,12,\"utf8 test Wörter auf Deutsch\"");

            //TSCLIB_DLL.about();
            byte status = TSCLIB_DLL.usbportqueryprinter();//0 = idle, 1 = head open, 16 = pause, following <ESC>!? command of TSPL manual
            TSCLIB_DLL.openport("TSC TE210");
            TSCLIB_DLL.sendcommand("SIZE 81 mm, 16 mm");
            //TSCLIB_DLL.sendcommand("SPEED 4");
            //TSCLIB_DLL.sendcommand("DENSITY 12");
            TSCLIB_DLL.sendcommand("DIRECTION 0,0");
            TSCLIB_DLL.sendcommand("REFERENCE 0,0");
            TSCLIB_DLL.sendcommand("OFFSET 0 mm");
            TSCLIB_DLL.sendcommand("SET PEEL OFF");
            TSCLIB_DLL.sendcommand("SET CUTTER OFF");
            TSCLIB_DLL.sendcommand("SET PARTIAL_CUTTER OFF");
            TSCLIB_DLL.sendcommand("GAP 3 mm, 0 mm");
            TSCLIB_DLL.sendcommand("SET TEAR ON");
            TSCLIB_DLL.sendcommand("CLS");
            TSCLIB_DLL.sendcommand("CODEPAGE 1252");
            TSCLIB_DLL.clearbuffer();

            //  TSCLIB_DLL.downloadpcx("UL.PCX", "UL.PCX");
            //  TSCLIB_DLL.windowsfont(645, 91, 0, 180, 11, 11, "Arial", "G.Wt :");
            //  TSCLIB_DLL.windowsfontUnicode(40, 550, 48, 0, 0, 0, "Arial", result_unicode);
            //  TSCLIB_DLL.sendcommand("PUTPCX 40,40,\"UL.PCX\"");
            //  TSCLIB_DLL.sendBinaryData(result_utf8, result_utf8.Length);
            //  TSCLIB_DLL.barcode("40", "300", "128", "80", "1", "0", "2", "2", B1);
            if (dt_printdata.Rows.Count > 0)
            {
                if (dt_printdata.Rows.Count >= 1)
                {
                    //TSCLIB_DLL. 
                    TSCLIB_DLL.sendcommand("BITMAP 29,9,61,56,1,ÿÿþ~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ÿÿÿÿÿþ>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ>ÿÿÿÿü?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿü?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿü?ÿÿÿÿù¾ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿù¾ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿù¾ÿÿÿÿÿóœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóœÿÿÿÿÿç˜ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿç˜ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿç˜ÿÿÿÿçÓÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÓÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÓÿÿÿÿÏÇ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÇ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÇ?ÿÿüÿŸÏ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿŸÏ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿŸÏ?ÿÿüÿÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿÿ¿ÿÿÿÿ'¼ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'¼ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'¼ÿÿ þgœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ þgœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ þgœÿÿ8÷Ÿàÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ8÷Ÿàÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ8÷Ÿàÿ¿	÷Ÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ¿	÷Ÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ¿	÷ŸþÏŸáóŸüÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏŸáóŸüÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏŸáóŸüÿ€ñóßùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ€ñóßùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ€ñóßùÿÈôróÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÈôróÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÈôróÿÏÀöp #ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÀöp #ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÀöp #ÿïÌ3ŸÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿïÌ3ŸÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿïÌ3ŸÏçÏ‡“Ÿ€ÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏ‡“Ÿ€ÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏ‡“Ÿ€ÏçÏã‡¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏã‡¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏã‡¿óïðƒ¾~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóïðƒ¾~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóïðƒ¾~óçø<üÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóçø<üÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóçø<üÿûäšqùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûäšqùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûäšqùÿùðø˜Ãóÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùðø˜Ãóÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùðø˜Ãóÿûãø çÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûãø çÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûãø çÿÿ‰ó#Ïÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ‰ó#Ïÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ‰ó#Ïÿÿ<Fùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ<Fùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ<Fùÿþ~ üyÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ üyÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ üyÿüüqàùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüüqàùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüüqàùÿùùä¸|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùùä¸|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùùä¸|ÿóóÌþ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóóÌþ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóóÌþ|ÿççÎ<þ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÎ<þ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÎ<þ|ÿ'ÏÞ?>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'ÏÞ?>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'ÏÞ?>0œž>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ0œž>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ0œž>~˜ÎC¿?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ~˜ÎC¿?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ~˜ÎC¿?þ@æp?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ@æp?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ@æp?üþòþ ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüþòþ ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüþòþ ?ùÿžøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùÿžøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùÿžøÿóÿžü?Ÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžü?Ÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžü?Ÿ¿óÿžyßÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžyßÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžyßÿàžsÀÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàžsÀÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàžsÀÏÿðÞgðÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿðÞgðÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿðÞgðÿÿñßOÿ‹ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñßOÿ‹ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñßOÿ‹ÿÿÿÏßñÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏßñÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏßñÿÿÿï?Ÿûÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿï?Ÿûÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿï?Ÿûÿÿÿî??ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿî??ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿî??ÿÿÿÿäžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿäžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿäžÿÿÿÿñžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñžÿÿÿÿóÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÜÿÿÿÿÿÿÉÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÉÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÉÿÿÿÿÿÿÃÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÃÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÃÿÿÿÿÿççÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ");
                    TSCLIB_DLL.printerfont("645", "91", "0", "180", "11", "11", dt_printdata.Rows[0]["print1"].ToString());
                    TSCLIB_DLL.printerfont("561", "89", "0", "180", "12", "12", dt_printdata.Rows[0]["print2"].ToString());
                    TSCLIB_DLL.printerfont("641", "52", "0", "180", "12", "12", dt_printdata.Rows[0]["print3"].ToString());
                }
                if (dt_printdata.Rows.Count >= 2)
                {
                    TSCLIB_DLL.sendcommand("BITMAP 29,9,61,56,1,ÿÿþ~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ÿÿÿÿÿþ>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ>ÿÿÿÿü?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿü?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿü?ÿÿÿÿù¾ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿù¾ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿù¾ÿÿÿÿÿóœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóœÿÿÿÿÿç˜ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿç˜ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿç˜ÿÿÿÿçÓÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÓÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÓÿÿÿÿÏÇ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÇ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÇ?ÿÿüÿŸÏ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿŸÏ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿŸÏ?ÿÿüÿÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿÿ¿ÿÿÿÿ'¼ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'¼ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'¼ÿÿ þgœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ þgœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ þgœÿÿ8÷Ÿàÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ8÷Ÿàÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ8÷Ÿàÿ¿	÷Ÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ¿	÷Ÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ¿	÷ŸþÏŸáóŸüÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏŸáóŸüÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏŸáóŸüÿ€ñóßùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ€ñóßùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ€ñóßùÿÈôróÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÈôróÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÈôróÿÏÀöp #ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÀöp #ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÀöp #ÿïÌ3ŸÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿïÌ3ŸÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿïÌ3ŸÏçÏ‡“Ÿ€ÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏ‡“Ÿ€ÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏ‡“Ÿ€ÏçÏã‡¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏã‡¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏã‡¿óïðƒ¾~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóïðƒ¾~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóïðƒ¾~óçø<üÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóçø<üÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóçø<üÿûäšqùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûäšqùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûäšqùÿùðø˜Ãóÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùðø˜Ãóÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùðø˜Ãóÿûãø çÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûãø çÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûãø çÿÿ‰ó#Ïÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ‰ó#Ïÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ‰ó#Ïÿÿ<Fùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ<Fùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ<Fùÿþ~ üyÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ üyÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ üyÿüüqàùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüüqàùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüüqàùÿùùä¸|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùùä¸|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùùä¸|ÿóóÌþ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóóÌþ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóóÌþ|ÿççÎ<þ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÎ<þ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÎ<þ|ÿ'ÏÞ?>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'ÏÞ?>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'ÏÞ?>0œž>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ0œž>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ0œž>~˜ÎC¿?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ~˜ÎC¿?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ~˜ÎC¿?þ@æp?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ@æp?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ@æp?üþòþ ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüþòþ ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüþòþ ?ùÿžøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùÿžøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùÿžøÿóÿžü?Ÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžü?Ÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžü?Ÿ¿óÿžyßÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžyßÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžyßÿàžsÀÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàžsÀÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàžsÀÏÿðÞgðÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿðÞgðÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿðÞgðÿÿñßOÿ‹ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñßOÿ‹ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñßOÿ‹ÿÿÿÏßñÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏßñÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏßñÿÿÿï?Ÿûÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿï?Ÿûÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿï?Ÿûÿÿÿî??ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿî??ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿî??ÿÿÿÿäžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿäžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿäžÿÿÿÿñžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñžÿÿÿÿóÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÜÿÿÿÿÿÿÉÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÉÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÉÿÿÿÿÿÿÃÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÃÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÃÿÿÿÿÿççÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ");
                    TSCLIB_DLL.printerfont("429", "91", "0", "180", "11", "11", dt_printdata.Rows[1]["print1"].ToString());
                    TSCLIB_DLL.printerfont("345", "89", "0", "180", "12", "12", dt_printdata.Rows[1]["print2"].ToString());
                    TSCLIB_DLL.printerfont("425", "52", "0", "180", "12", "12", dt_printdata.Rows[1]["print3"].ToString());
                }
                if (dt_printdata.Rows.Count == 3)
                {
                    TSCLIB_DLL.sendcommand("BITMAP 29,9,61,56,1,ÿÿþ~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ÿÿÿÿÿþ>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ>ÿÿÿÿü?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿü?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿü?ÿÿÿÿù¾ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿù¾ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿù¾ÿÿÿÿÿóœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóœÿÿÿÿÿç˜ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿç˜ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿç˜ÿÿÿÿçÓÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÓÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÓÿÿÿÿÏÇ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÇ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÇ?ÿÿüÿŸÏ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿŸÏ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿŸÏ?ÿÿüÿÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüÿÿ¿ÿÿÿÿ'¼ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'¼ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'¼ÿÿ þgœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ þgœÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ þgœÿÿ8÷Ÿàÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ8÷Ÿàÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ8÷Ÿàÿ¿	÷Ÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ¿	÷Ÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ¿	÷ŸþÏŸáóŸüÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏŸáóŸüÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏŸáóŸüÿ€ñóßùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ€ñóßùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ€ñóßùÿÈôróÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÈôróÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÈôróÿÏÀöp #ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÀöp #ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏÀöp #ÿïÌ3ŸÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿïÌ3ŸÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿïÌ3ŸÏçÏ‡“Ÿ€ÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏ‡“Ÿ€ÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏ‡“Ÿ€ÏçÏã‡¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏã‡¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçÏã‡¿óïðƒ¾~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóïðƒ¾~ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóïðƒ¾~óçø<üÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóçø<üÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóçø<üÿûäšqùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûäšqùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûäšqùÿùðø˜Ãóÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùðø˜Ãóÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùðø˜Ãóÿûãø çÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûãø çÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿûãø çÿÿ‰ó#Ïÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ‰ó#Ïÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ‰ó#Ïÿÿ<Fùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ<Fùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ<Fùÿþ~ üyÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ üyÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ~ üyÿüüqàùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüüqàùÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüüqàùÿùùä¸|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùùä¸|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùùä¸|ÿóóÌþ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóóÌþ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóóÌþ|ÿççÎ<þ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÎ<þ|ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÎ<þ|ÿ'ÏÞ?>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'ÏÞ?>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ'ÏÞ?>0œž>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ0œž>ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ0œž>~˜ÎC¿?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ~˜ÎC¿?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ~˜ÎC¿?þ@æp?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ@æp?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿþ@æp?üþòþ ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüþòþ ?ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿüþòþ ?ùÿžøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùÿžøÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿùÿžøÿóÿžü?Ÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžü?Ÿ¿ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžü?Ÿ¿óÿžyßÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžyßÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÿžyßÿàžsÀÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàžsÀÏÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿàžsÀÏÿðÞgðÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿðÞgðÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿðÞgðÿÿñßOÿ‹ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñßOÿ‹ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñßOÿ‹ÿÿÿÏßñÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏßñÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÏßñÿÿÿï?Ÿûÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿï?Ÿûÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿï?Ÿûÿÿÿî??ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿî??ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿî??ÿÿÿÿäžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿäžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿäžÿÿÿÿñžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñžÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿñžÿÿÿÿóÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿóÜÿÿÿÿÿÿÉÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÉÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÉÿÿÿÿÿÿÃÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÃÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÃÿÿÿÿÿççÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿççÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ÷ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ");
                    TSCLIB_DLL.printerfont("213", "91", "0", "180", "11", "11", dt_printdata.Rows[2]["print1"].ToString());
                    TSCLIB_DLL.printerfont("129", "89", "0", "180", "12", "12", dt_printdata.Rows[2]["print2"].ToString());
                    TSCLIB_DLL.printerfont("209", "52", "0", "180", "12", "12", dt_printdata.Rows[2]["print3"].ToString());
                }

                TSCLIB_DLL.printlabel("1", "1");
                TSCLIB_DLL.closeport();
            }
            else
            {
                MessageBox.Show("Printing data not available the minimum of records", "Alert");
                return;
            }
        }
        private void btnTagPrint_Click(object sender, EventArgs e)
        {
            if (dt_printdata != null && dt_printdata.Rows.Count > 0)
            {
                btnPrint_Sticker();
                dt_printdata = null;
            }
            else
            {
                MessageBox.Show("Please Save and Take Print", "Information");
            }
        }
        private void btnImgClear_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
        }
    }
}
