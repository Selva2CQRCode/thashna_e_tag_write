﻿namespace _2CQR
{
    using System;
    using System.IO;
    using System.Net;

    public class SelvaWebService
    {
        public bool checkNeeded(string epc)
        {
            return true;
        }

        public void ClosingComm()
        {
        }

        public string getRFIDTagData(string content)
        {
            return "";
        }

        public string importData(string filename)
        {
            throw new NotImplementedException();
        }

        public bool OnlineStsCheck()
        {
            return true;
        }

        public bool postSyncTotalData()
        {
            return true;
        }

        public bool Report(string query)
        {
            return true;
        }

        public bool ServerConnection()
        {
            return true;
        }

        public bool update(string[] tempContent)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create("https://2cqrauto.online/thasna/api/request/item_insert");
                request.ContentType = "application/x-www-form-urlencoded";
                request.Method = "POST";
                request.Proxy = null;
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    string str = "";
                    string[] textArray1 = new string[] { 
                        "{\"data\":[{\"item_type\":\"", tempContent[0], "\",\"no_of_stones\":\"", tempContent[1], "\",\"stone_r\": \"", tempContent[2], "\",\"stone_g\": \"", tempContent[3], "\",\"stone_w\": \"", tempContent[4], "\",\"stone_k\": \"", tempContent[5], "\",\"hall_no\": \"", tempContent[6], "\",\"extra_tag\": \"", tempContent[7], 
                        "\",\"extra_tag_value\": \"", tempContent[8], "\",\"sno\": \"", tempContent[9], "\",\"datetime\": \"", tempContent[10], "\",\"itemcode\": \"", tempContent[11], "\",\"img\": \"", tempContent[12], "\",\"stone_wt\": \"", tempContent[13], "\",\"gross_wt\": \"", tempContent[14], "\",\"net_wt\": \"", tempContent[15],
                        "\",\"rfid_tag_wt\":\"",tempContent[16],"\",\"wastage_k\":\"",tempContent[17],"\",\"melting_k\": \"", tempContent[18], "\",\"wastage_d\": \"", tempContent[19], "\",\"melting_d\": \"", tempContent[20], "\",\"actual_stone\": \"", tempContent[21], "\"}]}"
                     };
                    str = string.Concat(textArray1);
                    writer.Write(str);
                }
                HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                if (response.StatusCode.ToString() == "OK")
                {
                    response.Close();
                    return true;
                }
                response.Close();
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool updateSync(string content)
        {
            return true;
        }
    }
}

